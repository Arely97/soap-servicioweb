package com.example.materias

import android.util.Log
import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE

class CallWebService {
    fun callApi(
        methodName: String,
        input1: String?,
        input2: String?,
        input3: String?,
        input4: String?,
        input5: String?
    ): String {
        var result = ""
        val soapaction = "http://ServicioWebSoap2.somee.com/Add"
        val soapObject = SoapObject(Utils.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("intA", input1)
        soapObject.addProperty("intB", input2)
        soapObject.addProperty("intC", input3)
        soapObject.addProperty("intD", input4)
        soapObject.addProperty("intE", input5)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(Utils.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result = soapPrimitive.toString()
            result

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result
    }

}