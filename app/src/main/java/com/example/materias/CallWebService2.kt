package com.example.materias

import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE

class CallWebService2 {

    fun callApi2(
        methodName: String,
        sumas: String?

    ): String {
        var result1 = ""
        val soapaction = "http://ServicioWebSoap2.somee.com/promedio1"
        val soapObject = SoapObject(Utils.SOAP_NAMESPACE, methodName)
        soapObject.addProperty("suma",sumas)

        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(Utils.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result1 = soapPrimitive.toString()
            result1

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result1
    }
}