package com.example.materias

import org.ksoap2.SoapEnvelope
import org.ksoap2.serialization.SoapObject
import org.ksoap2.serialization.SoapSerializationEnvelope
import org.ksoap2.transport.HttpTransportSE

class CallWebService4 {
    fun callApi4(
        methodName: String,
valor:String?

    ): String {
        var result2 = ""
        val soapaction = "http://ServicioWebSoap2.somee.com/desvia"
        val soapObject = SoapObject(Utils.SOAP_NAMESPACE, methodName)


        soapObject.addProperty("varianza1", valor)


        val envelope = SoapSerializationEnvelope(SoapEnvelope.VER11)
        envelope.setOutputSoapObject(soapObject)

        envelope.dotNet = true


        val httpTransportSE = HttpTransportSE(Utils.SOAP_URL)

        return try {
            httpTransportSE.call(soapaction, envelope)
            val soapPrimitive = envelope.response
            result2 = soapPrimitive.toString()
            result2

        } catch (e: Exception) {
            e.printStackTrace()
            "Error!"
        }
        return result2
    }
}