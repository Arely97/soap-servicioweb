package com.example.materias


import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var input1: String
        var input2: String
        var input3: String
        var input4: String
        var input5: String

        add.setOnClickListener {
            input1 = enterInput1.text.toString().trim()
            input2 = enterInput2.text.toString().trim()
            input3 = enterInput3.text.toString().trim()
            input4 = enterInput4.text.toString().trim()
            input5 = enterInput5.text.toString().trim()
            if (input1.length == 0 || input2.length == 0 || input3.length == 0 || input4.length == 0 || input5.length == 0) {
                Toast.makeText(
                    this,
                    getString(R.string.fill_field),
                    Toast.LENGTH_SHORT
                ).show()
                !Utils.isConnected(this@MainActivity)
                Toast.makeText(
                    this,
                    getString(R.string.no_internet),
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                getSuma().execute(input1, input2, input3, input4, input5)
            }
        }
    }

    inner class getSuma : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = CallWebService().callApi(
                Utils.METHOD_ADD,
                params[0],
                params[1],
                params[2],
                params[3],
                params[4]

            )
            Log.v("response", "response==$response")
            return response
        }

        public override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            Log.v("response", "OnPostresponse==$result")
            try {
                resultValue.text = result
                getPro().execute(result)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getPro : AsyncTask<String, String, String>() {
        public override fun doInBackground(vararg params: String?): String {
            val response = CallWebService2().callApi2(
                Utils.METHOD_PRO,
                params[0]
            )
            Log.v("response", "response==$response")
            return response
        }


        public override fun onPostExecute(result1: String?) {
            super.onPostExecute(result1)
            Log.v("response", "OnPostresponse==$result1")
            try {
                resultValue1.text = result1
                getCali1().execute(
                    this@MainActivity.enterInput1.text.toString(),
                    this@MainActivity.enterInput2.text.toString(),
                    this@MainActivity.enterInput3.text.toString(),
                    this@MainActivity.enterInput4.text.toString(),
                    this@MainActivity.enterInput5.text.toString(),
                    result1
                )
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

    }

    inner class getCali1 : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = CallWebService3().callApi3(
                Utils.METHOD_CALIPRO,
                params[0],
                params[1],
                params[2],
                params[3],
                params[4],
                params[5]
            )
            Log.v("response", "response==$response")
            return response
        }

        public override fun onPostExecute(result2: String?) {
            super.onPostExecute(result2)
            Log.v("response", "OnPostresponse==$result2")
            try {
                resultValue2.text = result2
                getCali2().execute(result2)
                Log.v("ok", "ok==$result2")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }

    inner class getCali2 : AsyncTask<String, String, String>() {

        public override fun doInBackground(vararg params: String?): String {
            val response = CallWebService4().callApi4(
                Utils.METHOD_CALIPRO1,
                params[0]
            )
            Log.v("response", "response==$response")
            return response
        }

        public override fun onPostExecute(result3: String?) {
            super.onPostExecute(result3)
            Log.v("response", "OnPostresponse==$result3")
            try {
                resultValue3.text = result3
                Log.v("ok", "ok==$result3")
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }
    }
}