package com.example.materias

import android.content.Context
import android.net.ConnectivityManager

class Utils {
    companion object {

        const val SOAP_URL = "http://serviciowebsoap2.somee.com/Suma.asmx?"
        const val SOAP_NAMESPACE = "http://ServicioWebSoap2.somee.com"
        const val METHOD_ADD = "Add"
        const val METHOD_PRO = "promedio1"
        const val METHOD_CALIPRO = "Varianza"
        const val METHOD_CALIPRO1 = "desvia"
        fun isConnected(context: Context): Boolean {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork = cm.activeNetworkInfo
            return activeNetwork != null && activeNetwork.isConnectedOrConnecting
        }
    }
}